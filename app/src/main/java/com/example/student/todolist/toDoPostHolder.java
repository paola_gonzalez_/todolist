package com.example.student.todolist;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class toDoPostHolder extends RecyclerView.ViewHolder{
    public TextView titleText;

    public toDoPostHolder(View itemView) {
        super(itemView);
        titleText = (TextView)itemView;
    }
}
