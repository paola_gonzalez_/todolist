package com.example.student.todolist;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class ItemFragment extends Fragment {

    private RecyclerView recyclerView;
    private ActivityCallback activityCallback;
    private MainActivity activity;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        activityCallback = (ActivityCallback)activity;
        this.activity = (MainActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        activityCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        toDoItem i1 = new toDoItem("hi", "hello", "hey", "hi");
        toDoItem i2 = new toDoItem("no", "NO", "No", "no");
        toDoItem i3 = new toDoItem("why", "?", "why", "?");
        toDoItem i4 = new toDoItem("what", "?", "whta", "whz");
        toDoItem i5 = new toDoItem("nah", "?", "?", "!");

        activity.toDoItems.add(i1);
        activity.toDoItems.add(i2);
        activity.toDoItems.add(i3);
        activity.toDoItems.add(i4);
        activity.toDoItems.add(i5);

        return view;
    }

}
