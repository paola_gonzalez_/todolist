package com.example.student.todolist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class toDoPostAdapter extends RecyclerView.Adapter<toDoPostHolder> {
    private String[] todoPosts;
    public toDoPostAdapter(String[] todoPosts) {

        this.todoPosts = todoPosts;
    }

    @Override
    public toDoPostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new toDoPostHolder(view);
    }

    @Override
    public void onBindViewHolder(toDoPostHolder holder, int position) {
        holder.titleText.setText(todoPosts[position]);
    }

    @Override
    public int getItemCount() {
        return todoPosts.length;
    }
}
